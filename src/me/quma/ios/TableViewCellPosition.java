package me.quma.ios;

public class TableViewCellPosition {
	
	public int section;
	public int row;
	
	public TableViewCellPosition(int sec, int row) {
		this.section = sec;
		this.row = row;
	}
}
