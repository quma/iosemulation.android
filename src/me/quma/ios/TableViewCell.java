package me.quma.ios;

import android.content.Context;
import android.content.res.Resources;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class TableViewCell extends RelativeLayout {
	
	public TableViewCell(Context ctx, AttributeSet attrs) {
		super(ctx, attrs);
	}
	
	@SuppressWarnings("deprecation")
	public TableViewCell(Context ctx, boolean isFirstRow, boolean isLastRow, boolean hasSectionTitle) {
		super(ctx);
		
		LayoutInflater inflater = (LayoutInflater)ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		inflater.inflate(R.layout.list_row, this, true);

		LinearLayout contentLayout = (LinearLayout)findViewById(R.id.cell_content_layout);
		
		Resources res = getResources();
		float d = res.getDisplayMetrics().density;
		int m = (int)(10 * d);
		
		if (isFirstRow && !isLastRow) { // top row
			contentLayout.setBackgroundDrawable(res.getDrawable(R.drawable.grouped_cell_t));			
			if (hasSectionTitle)
				setPadding(m, 0, m, 0);
			else 
				setPadding(m, m, m, 0);
		} else if (isFirstRow && isLastRow) { // top AND bottom row
			contentLayout.setBackgroundDrawable(res.getDrawable(R.drawable.grouped_cell_tb));
			if (hasSectionTitle)
				setPadding(m, 0, m, m);
			else 
				setPadding(m, m, m, m);
		} else if (!isFirstRow && isLastRow) { // bottom row
			contentLayout.setBackgroundDrawable(res.getDrawable(R.drawable.grouped_cell_b));
			setPadding(m, 0, m, m);
		} else {
			setPadding(m, 0, m, 0);
		}
	}
	
	public void setTitle(String title) {
		TextView tv = (TextView)findViewById(R.id.cell_title_textview);
		tv.setText(title);
	}
	
	public void setImageResource(int id) {
		ImageView iv = (ImageView)findViewById(R.id.cell_imageview);
		if (id == 0) // special
			iv.setVisibility(View.GONE);
		else {
			iv.setVisibility(View.VISIBLE);
			iv.setImageResource(id);
		}
	}
	
}
