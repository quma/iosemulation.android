package me.quma.ios;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class TableViewAdapter extends BaseAdapter {

	private static final int SECTION_TITLE_MAGIC_ROW_NUM = -1;
	
	private BaseTableViewDataSource mSource;
	private int[] mNumberOfRows;
	private LayoutInflater mInflater;
	private Context mCtx;
	private String[] mSectionTitles;
	
	private Integer mCustomCellLayoutId = null;
	private Integer mCustomCellFirstRowLayoutId = null;
	private Integer mCustomCellLastRowLayoutId = null;
	
	public TableViewAdapter(Context ctx, BaseTableViewDataSource source) {
		
		mSource = source;
		mCtx = ctx;
		mInflater = (LayoutInflater)ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		
		reloadData();
	}
	
	public void reloadData() {
		if (mSource == null) return;
		
		int sections = mSource.numberOfSections(this);
		mNumberOfRows = new int[sections];
		mSectionTitles = new String[sections];
		
		for (int sec = 0; sec < mNumberOfRows.length; sec++) {
			mNumberOfRows[sec] = mSource.numberOfRowsInSection(this, sec);
			mSectionTitles[sec] = mSource.titleForSection(this, sec);
		}
		
		this.notifyDataSetChanged();
	}
	
	@Override
	public int getCount() {
		if (mNumberOfRows == null) return 0;
		
		int count = 0;
		for (int sec = 0; sec < mNumberOfRows.length; sec++) {
			count += (mSectionTitles[sec] != null) ? 1 : 0;
			count += mNumberOfRows[sec];
		}
		return count;
	}

	@Override
	public Object getItem(int position) {
		if (mNumberOfRows == null) return null;
		
		TableViewCellPosition cellPos = getCellPositionForPosition(position);
		if (cellPos != null) {
			if (cellPos.row == SECTION_TITLE_MAGIC_ROW_NUM)
				return mSectionTitles[cellPos.section];
			return mSource.itemForCellAt(this, cellPos);
		}
		return null;
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View view, ViewGroup parent) {
		if (mSource == null) return null;
		
		TableViewCellPosition cellPos = getCellPositionForPosition(position);
		if (cellPos != null) {
			if (cellPos.row == SECTION_TITLE_MAGIC_ROW_NUM) {
				TextView tv = (TextView)mInflater.inflate(R.layout.section_header, null);
				tv.setText(mSectionTitles[cellPos.section]);
				return tv;
			} else {
				boolean isFirstRow = (cellPos.row == 0);
				boolean isLastRow = (cellPos.row == mNumberOfRows[cellPos.section]-1);
				return mSource.viewForCellAt(this, cellPos, isFirstRow, isLastRow);
			}
		}
		
		return null;
	}
	
	////////////////////////////////////////////////////////////
	// Public Interfaces
	
	public void setCustomCellLayoutId(int id) { mCustomCellLayoutId = id; }
	public void setCustomCellFirstRowLayoutId(int id) { mCustomCellFirstRowLayoutId = id; }
	public void setCustomCellLastRowLayoutId(int id) { mCustomCellLastRowLayoutId = id; }
	
	public ViewGroup standardCellViewForPosition(TableViewCellPosition cellPos) {
		
		if (cellPos.row == SECTION_TITLE_MAGIC_ROW_NUM) 
			return null; // this should not happen at all
		
		boolean isFirstRow = (cellPos.row == 0);
		boolean isLastRow = (cellPos.row == mNumberOfRows[cellPos.section]-1);
		boolean hasSectionTitle = (mSectionTitles[cellPos.section] != null);
		
		LayoutInflater inflater = (LayoutInflater)mCtx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		
		if (isFirstRow) {
			if (mCustomCellFirstRowLayoutId != null) {
				return (ViewGroup)inflater.inflate(mCustomCellFirstRowLayoutId, null);
			}
			if (mCustomCellLayoutId != null) {
				return (ViewGroup)inflater.inflate((int)mCustomCellLayoutId, null);
			}
		} else if (isLastRow) {
			if (mCustomCellLastRowLayoutId != null) {
				return (ViewGroup)inflater.inflate(mCustomCellLastRowLayoutId, null);
			}
			if (mCustomCellLayoutId != null) {
				return (ViewGroup)inflater.inflate(mCustomCellLayoutId, null);
			}
		} else {
			if (mCustomCellLayoutId != null) {
				return (ViewGroup)inflater.inflate(mCustomCellLayoutId, null);
			}
		}
		return new TableViewCell(mCtx, isFirstRow, isLastRow, hasSectionTitle);
	}

	public TableViewCellPosition getCellPositionForPosition(int position) {
		for (int sec = 0; sec < mNumberOfRows.length; sec++) {
			if (mSectionTitles[sec] !=null ) { // has section title, need to take care of this
				if (position == 0) {
					return new TableViewCellPosition(sec, SECTION_TITLE_MAGIC_ROW_NUM);
				} else {
					position --;
				}
			} 
			
			if (position < mNumberOfRows[sec]) {
				return new TableViewCellPosition(sec, position);
			}
			position -= mNumberOfRows[sec];
		}
		return null;
	}

}
