package me.quma.ios;

import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.Context;
import android.content.DialogInterface;
import android.text.InputType;
import android.util.SparseBooleanArray;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

public class UtilDialog {
	
	public interface CompletionHandler<T> { 
		public void onComplete(T value, boolean cancelled);
	}

	public static void showTextInputDialog(Context ctx, String title, String value, final CompletionHandler<String> handler) {
		show(ctx, title, "", value, InputType.TYPE_CLASS_TEXT, handler);
	}

	public static void showIntegerInputDialog(Context ctx, String title, String value, final CompletionHandler<String> handler) {
		show(ctx, title, "", value, InputType.TYPE_CLASS_NUMBER, handler);
	}
	
	public static void showDecimalInputDialog(Context ctx, String title, String value, final CompletionHandler<String> handler) {
		show(ctx, title, "", value, InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_DECIMAL, handler);
	}

	public static void showAlertDialog(Context ctx, String title, String message) {
		new AlertDialog.Builder(ctx).setTitle(title).setMessage(message)
		.setPositiveButton("OK", new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) { dialog.dismiss(); }
		}).show();
	}
	
	public static void showAlertDialog(Context ctx, int titleId, int messageId) {
		new AlertDialog.Builder(ctx)
		.setTitle(ctx.getString(titleId))
		.setMessage(ctx.getString(messageId))
		.setPositiveButton("OK", new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) { dialog.dismiss(); }
		}).show();
	}

	public static void showAlertDialog(Context ctx, String title, String message, final CompletionHandler<Void> handler) {
		new AlertDialog.Builder(ctx).setTitle(title).setMessage(message)
		.setPositiveButton("OK", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int which) {
				dialog.dismiss();
				if (handler != null) {
					handler.onComplete(null, false);
				}
			}
		}).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int which) {
				dialog.dismiss();
				if (handler != null) {
					handler.onComplete(null, true);
				}
			}
		}).show();
	}
	
	public static void showSingleSelectDialog(Context ctx,
			String title, String[] choices, final CompletionHandler<Integer> handler) {
		AlertDialog.Builder b = new Builder(ctx);
		b.setTitle(title);		
		b.setItems(choices, new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				handler.onComplete(which, false);
			}
		});
		b.show();
	}
	
	public static void showMultiSelectDialog(Context ctx, 
			String title, String okButtonText, String cancelButtonText,
			String[] choices, 
			boolean[] states, 
			final CompletionHandler<SparseBooleanArray> handler) {
		AlertDialog.Builder b = new Builder(ctx);
		b.setTitle(title);		
		b.setMultiChoiceItems(choices, states, new DialogInterface.OnMultiChoiceClickListener() {
			public void onClick(DialogInterface dialog, int hich, boolean state) {}
		});
		
		b.setPositiveButton(okButtonText, new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				handler.onComplete(((AlertDialog)dialog).getListView().getCheckedItemPositions(), false);
				//if(CheCked.get(0) == true){
			}
		});
		b.setNegativeButton(cancelButtonText, new DialogInterface.OnClickListener() {
	        public void onClick(DialogInterface dialog, int id) {
	             dialog.cancel();
	             handler.onComplete(null, true);
	        }
	    });
		b.show();
	}
	
	private static void show(Context ctx, String title, String message, String value, int inputType, final CompletionHandler<String> handler) {
		final EditText input = new EditText(ctx);
		input.setText(value);
		input.setInputType(inputType);
		
		final InputMethodManager imm = (InputMethodManager)ctx.getSystemService(Context.INPUT_METHOD_SERVICE);
		
		new AlertDialog.Builder(ctx).setTitle(title).setMessage(message).setView(input)
		.setPositiveButton("OK", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int which) {
				dialog.dismiss();
				imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
				
				String value = input.getText().toString();
				if (handler != null) {
					handler.onComplete(value, false);
				}
			}
		}).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int which) {
				dialog.dismiss();
				imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
				if (handler != null) {
					handler.onComplete(null, true);
				}
			}
		}).show();

		input.requestFocus();
		imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
	}
}
