package me.quma.ios;

import android.view.View;

public abstract class BaseTableViewDataSource {

	public int numberOfSections(TableViewAdapter adapter) {
		return 1;
	}

	public abstract int numberOfRowsInSection(TableViewAdapter adapter, int section);

	public abstract View viewForCellAt(TableViewAdapter adapter,
			TableViewCellPosition pos, boolean isFirstRowInSection,
			boolean isLastRowInSection); 

	public Object itemForCellAt(TableViewAdapter adapter,
			TableViewCellPosition pos) {
		return null;
	}

	public String titleForSection(TableViewAdapter adapter, int section) {
		return null;
	}

}
