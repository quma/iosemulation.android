package me.quma.ios;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

public class BaseActivity extends Activity {

	protected boolean mWasPushedIn; // set to true if the activity was pushed in
	protected BaseActivity self = this;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		Bundle b = getIntent().getExtras();
		if (b != null && b.containsKey("isPushedIn")) {
			mWasPushedIn = true;
		}
	}
	
	public void pushIntent(Intent i) {
		i.putExtra("isPushedIn", true);
		startActivity(i);
		overridePendingTransition(R.anim.slide_in_from_left, R.anim.stationary);
	}
	
	public void popActivity() {
	    super.onBackPressed();	 
	    overridePendingTransition(R.anim.stationary, R.anim.slide_out_to_right);
	}

	@Override
	public void onBackPressed() {
		if (mWasPushedIn) popActivity();
		else super.onBackPressed();
	}	
}
